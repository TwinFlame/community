me: "11:11:11 Time Warp

https://www.skyscraper.org/TALLEST_TOWERS/t_wtc.htm
110
110
11 = 4+7 Building 7

Three Pillars of Freemasonry, the third one is "hidden"

"When completed in 1972, 1 World Trade Center became the tallest building in the world for two years, surpassing the Empire State Building after a 40-year reign. The North Tower stood 1,368 feet (417 m) tall and featured a telecommunications antenna or mast that was added at the top of the roof in 1978 and stood 362 feet (110 m) tall. With the 362-foot (110 m)-tall antenna/mast, the highest point of the North Tower reached 1,730 feet (530 m).[89] Chicago's Sears Tower, finished in May 1973, reached 1,450 feet (440 m) at the rooftop.[91] Throughout their existence, the WTC towers had more floors (at 110) than any other building.[89] This number was not surpassed until the advent of the Burj Khalifa, which opened in 2010.[92][93] Each tower had a total mass of around 500,000 tons.[94]""